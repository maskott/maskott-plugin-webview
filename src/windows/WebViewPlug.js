var cordova = require('cordova'), WebView = require('./WebView');
module.exports = {
	navigate: function (win, fail, strInput) {
		wbvw = strInput[0];
		path = strInput[1];
		path = path.replace(/ms-appdata:\/\//, '');
		var uriResolver = wbvw.buildLocalStreamUri('MyTag', path);
		var resolver = new MaskottWebViewHelperWinRT.StreamUriWinRTResolver();
		wbvw.navigateToLocalStreamUri(uriResolver, resolver);
		win(0);
	},
	getOfflineData: function (win, fail, strInput) {
	    wbvw = strInput[0];
	    data = strInput[1];	    
	    var asyncOp = wbvw.invokeScriptAsync("eval", "getOfflineData(" +JSON.stringify(data)+ ")");
	    asyncOp.start();
		win(0);
	}
};
require("cordova/exec/proxy").add("WebView", module.exports);
