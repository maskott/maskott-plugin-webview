var exec = cordova.require('cordova/exec');

exports.navigate = function(webview, uri, callback) {
    var win = function(result) {
        if (callback) {
            callback(0);
        }
    };
    var fail = function(result) {
        if (callback) {
            callback(-1);
        }
    };
    exec(win, fail, 'WebView', 'navigate', [webview, uri]);
};
exports.getOfflineData = function(webview, data, callback) {
    var win = function(result) {
        if (callback) {
            callback(0);
        }
    };
    var fail = function(result) {
        if (callback) {
            callback(-1);
        }
    };
    exec(win, fail, 'WebView', 'getOfflineData', [webview, data]);
};
